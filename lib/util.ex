defmodule Util do
  def avg(as) do
    cond do
      Enum.empty? as ->
        0
      true ->
        {sum, len} = Enum.reduce(as, {0, 0}, fn(a, {sum, len}) -> {sum + a, len + 1} end)
          sum / len
    end
  end

  def std_dev(as) do
    avg_as = avg as
    as |> Enum.map(&abs(&1-avg_as)) |> avg
  end
end

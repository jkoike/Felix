defmodule Vector do
  use Rustler, otp_app: :felix, crate: :vector

  def mode do
    "elixir"
  end

  def from_list(_xs) do
    {:error, "lib not loaded"}
  end

  def to_list(_xs) do
    {:error, "lib not loaded"}
  end

  def z_order(_xs, _ys) do
    {:error, "lib not loaded"}
  end

  def dot(a, b) do
    Stream.zip(a, b)
    |> Enum.reduce(0, fn({l, r}, acc) -> acc + (l * r) end)
  end

  def dist(a, b) do
    dist_2(a, b)
    |> :math.sqrt
  end

  def cardinality(a) do
    :math.sqrt(Enum.reduce(a, 0, fn(n, acc) -> acc + :math.pow(n, 2) end))
  end

  def angle_cos(a, b) do
    dot(a, b) / (cardinality(a) * cardinality(b))
  end

  def add(a, b) do
    Stream.zip(a, b) |> Enum.map(fn({a, b}) -> _round_zero(a + b) end)
  end

  def sub(a, b) do
    add(a, scale(b, -1))
  end

  def scale(as, b) do
    as |> Enum.map(&_round_zero(&1 * b))
  end

  def project(u, v) do
    scale(u, dot(v, u)/dot(u, u))
  end

  def project_space(vs, u) do
    vs
    |> orthogonal
    |> Stream.map(&project(&1, u))
    |> Enum.reduce(&add/2)
  end

  def orthogonal([a|as]) do
    as
    |> Enum.reduce([a], fn(a, acc) ->
      [sub(a, acc |> Enum.map(&project(&1, a)) |> Enum.reduce(&add/2)) | acc] end)
  end

  def transpose(rows) do
    rows
    |> List.zip
    |> Enum.map(&Tuple.to_list/1)
  end

  def swap(as, a, b) do
    as
    |> Enum.to_list
    |> List.replace_at(a, Enum.at(as, b))
    |> List.replace_at(b, Enum.at(as, a))
  end

  def gaussian([]) do
    []
  end

  def gaussian(as) do
    [x | xs] = Enum.sort(as, &_sort_rows/2)
    factor = _lead_factor x
    unit = cond do
      factor == 0 -> x
      true        -> scale(x, -1/factor)
    end
    [x | xs |> Enum.map(&add(&1, scale(unit, _lead_factor &1))) |> gaussian]
  end


  def dist_2(as, bs) do
    Stream.zip(as, bs)
    |> Enum.reduce(0, fn({a, b}, acc) -> acc + :math.pow(a - b, 2) end)
  end

  defp _sort_rows(as, bs) do
    cond do
      _count_lead_zeroes(as) == _count_lead_zeroes(bs) ->
        (as |> Enum.filter(&(&1 != 0)) |> Util.std_dev)
        >= (bs |> Enum.filter(&(&1 != 0)) |> Util.std_dev)
      true ->
        _count_lead_zeroes(as) >= _count_lead_zeroes(bs)
    end
  end

  defp _count_lead_zeroes([x | xs]) do
    cond do
      abs(x) < _zero -> 
        1 + _count_lead_zeroes(xs)
      true -> 0
    end
  end

  defp _count_lead_zeroes(_) do
    0
  end

  defp _lead_factor([a | as]) do
    cond do
      abs(a) < _zero ->
        _lead_factor as
      true ->
        a
    end
  end

  defp _lead_factor([]) do
    0
  end

  defp _round_zero(a) do
    cond do
      abs(a) < _zero -> 0
      true -> a
    end
  end

  defp _zero do
    1.0e-10
  end

  def random_vector do
    Stream.iterate(:rand.normal / 0.1, fn(_x) -> :rand.normal / 0.1 end)
    |> Stream.take(300)
    |> Enum.to_list
    |> Vector.from_list
  end
end

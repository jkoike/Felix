defmodule VP do
  def find_leaf(point, db) do
    [head] = :dets.lookup(db, "<HEAD>")
    find_leaf(point, head, db)
  end

  def find_leaf(point, step, db) do
    {_word, {vantage, limit, inner, outer}} = step
    if Vector.dist(point, vantage) < limit do 
      case inner do
        nil -> step
        _   -> 
          [next] = :dets.lookup(db, inner)
          find_leaf(point, next, db)
      end
    else
      case outer do
        nil -> step
        _   ->
          [next] = :dets.lookup(db, outer)
          find_leaf(point, next, db)
      end
    end
  end

  def insert({word, point}, db) do
    {parent, {vantage, limit, inner, outer}} = find_leaf(point, db)
    next_limit = Vector.dist(point, vantage)
    if next_limit < limit do
      :dets.insert(db, {parent, {vantage, limit, word, outer}})
    else
      :dets.insert(db, {parent, {vantage, limit, inner, word}})
    end
    :dets.insert(db, {word, {point, next_limit, nil, nil}})
  end

  def search(word, db) do
    [head] = :dets.lookup(db, "<HEAD>")
    [{_word, {point, _limit, _inner, _outer}}] = :dets.lookup(db, word)
    {closest_word, {closest_point, _limit, _inner, _outer}} = search(point, head, db)
    {Vector.dist(point, closest_point), word, closest_word}
  end

  def search(_point, {word, {node, limit, nil, nil}}, _db) do
    {word, {node, limit, nil, nil}}
  end

  def search(point, step, db) do
    {_word, {node, limit, inner, outer}} = step
    dist = Vector.dist(point, node)
    cond do
      inner != nil and dist < limit ->
        [next] = :dets.lookup(db, inner)
        best = search(point, next, db)
        {_w, {v, _l, _i, _o}} = best
        best_dist = Vector.dist(v, point)
        cond do
          best_dist < 1.0e-10 -> step
          outer != nil and limit - dist < best_dist -> 
            [next] = :dets.lookup(db, outer)
            next_best = search(point, next, db)
            {_w, {v, _l, _i, _o}} = next_best
            if Vector.dist(v, point) < best_dist do
              next_best
            else
              best
            end
          true -> best
        end
      outer != nil ->
        [next] = :dets.lookup(db, outer)
        best = search(point, next, db)
        {_w, {v, _l, _i, _o}} = best
        best_dist = Vector.dist(v, point)
        cond do
          best_dist < 1.0e-10 -> step
          inner != nil and limit - dist < best_dist -> 
            [next] = :dets.lookup(db, inner)
            next_best = search(point, next, db)
            {_w, {v, _l, _i, _o}} = next_best
            if Vector.dist(v, point) < best_dist do
              next_best
            else
              best
            end
          true -> best
        end
      true -> step
    end
  end

  def test(word) do
    {:ok, db} = :dets.open_file("data/glove.6B.vertices.dets", [access: :read])
    IO.inspect search(word, db)
  end
end

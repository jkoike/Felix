defmodule Felix.Mixfile do
  use Mix.Project

  def project do
    [app: :felix,
     version: "0.1.0",
     elixir: "~> 1.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     compilers: [:rustler | Mix.compilers],
     rustler_crates: rustler_crates(),
     deps: deps()]
  end

  defp rustler_crates do
    [ vector: [
      path: "native/vector",
      mode: (if Mix.env == :prod, do: :release, else: :debug)
    ]
    ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:logger]]
  end

  defp deps do
    [ 
      {:alchemy, git: "https://github.com/cronokirby/alchemy.git"},
      {:rustler, "~>0.10"},
      {:excheck, "~>0.5", only: :test},
      {:triq, github: "triqng/triq", only: :test}
    ]
  end
end

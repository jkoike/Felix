defmodule ExCheck.CheckZorder do
  use ExUnit.Case, async: false
  use ExCheck
  alias Vector

  property :zorder_irreflexive do
    for_all _x in int() do
      v = Vector.random_vector
      abs(Vector.z_order(v, v)) < 1.0e-10
    end
  end

  property :zorder_asymmetric do
    for_all _x in int() do
      a = Vector.random_vector
      b = Vector.random_vector
      if Vector.z_order(a, b) < 1.0e-10 do
        Vector.z_order(b, a) > -1.0e-10
      else
        Vector.z_order(b, a) < -1.0e-10
      end
    end
  end

  property :zorder_transitive do
    for_all _x in int() do
      a = Vector.random_vector
      b = Vector.random_vector
      c = Vector.random_vector
      if Vector.z_order(a, b) > -1.0e-10 and Vector.z_order(b, c) > -1.0e-10 do
        Vector.z_order(a, c) > -1.0e-10
      else
        true
      end
    end
  end
  
  property :zorder_equate do
    for_all _x in int() do
      a = Vector.random_vector
      b = Vector.random_vector
      if abs(Vector.z_order(a, b)) < 1.0e-10 do
        abs(Vector.dist(a, b)) < 1.0e-10
      else
        abs(Vector.dist(a, b)) > 1.0e-10
      end
    end
  end
end
